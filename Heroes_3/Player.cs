﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_3
{
    class Player 
    {
        private ConsoleColor color;
        public List<Unit> units;
        public int gold; 
        public Player(ConsoleColor color)
        {
            this.color = color;
            units = new List<Unit>();
            gold = 70;
        }

        public ConsoleColor Color 
        {
            get
            {
                return color;
            }
        }

        public int Gold
        {
            get
            {
                return gold;
            }
        }

        public void BuyUnit(Unit unit) 
        {
            if (gold >= unit.Cost) 
            {
                gold -= unit.Cost; 
                units.Add(unit); 
            }
            else
            {
                Console.WriteLine("Недостаточно средств!");
                Console.ReadKey();
            }
        }

        public void DrawUnits(Player player) 
        {
            foreach (Unit unit in units) 
            {
                unit.Draw(player.Color);
            }
        }

    }

}
