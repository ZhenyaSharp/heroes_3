﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_3
{
    class Unit
    {
        private int x, y; 
        private string name; 
        private int damage;
        private char view; 
        private int stepDistance;  
        private int rangeAttack;  
        private int cost; 

        protected Unit(int x, int y, string name, int damage, int hp, char view, int stepDistance, int rangeAttack, int cost) 
        {
            this.x = x;
            this.y = y;
            this.name = name;
            this.damage = damage;
            this.Hp = hp;
            this.view = view;
            this.stepDistance = stepDistance;
            this.rangeAttack = rangeAttack;
            this.cost = cost;
        }


        public void Draw(ConsoleColor color)  
        {
            Console.ForegroundColor = color;  
            Console.SetCursorPosition(x, y);
            Console.Write(view); 
        }

        public int Cost 
        {
            get
            {
                return cost;
            }
        }

        public int Hp 
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }


        public override string ToString()
        {
            return $"{name} X:{x} У:{y} ХП:{Hp} Урон:{damage}\n";
        }

        public int X
        {
            get
            { return x; }
        }

        public int Y
        {
            get
            { return y;}
        }

        public int RangeAttack
        {
            get
            { return rangeAttack; }
        }

        private int StepDistance
        {
            get
            { return stepDistance; }
        }

        public int Damage
        {
            get
            { return damage; }
        }

        public void Attack(Unit unit)
        {
            unit.Hp -= damage;
        }

        public void MoveX(Unit unit)
        {
            unit.x += StepDistance;
        }

        public void MoveX2(Unit unit)
        {
            unit.x -= StepDistance;
        }

        public void MoveY(Unit unit)
        {
            unit.y += StepDistance;
        }

        public void MoveY2(Unit unit)
        {
            unit.y -= StepDistance;
        }

    }
}
