﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Heroes_3
{
    class GameManager
    {
        private Player P1;
        private Player P2;
        private int xMin, xMax, yMin, yMax;

        public GameManager()
        {
            xMin = 0;
            xMax = 21;
            yMin = 0;
            yMax = 16;
            P1 = new Player(ConsoleColor.Red);
            P2 = new Player(ConsoleColor.Blue);
        }

        private void InterfaceBuyUnit(Player player) 
        {
            int countStep = 0; 

            do
            {
                Console.Clear();
                Console.SetCursorPosition(20, 0);
                Console.ForegroundColor = player.Color;
                Console.WriteLine($"{player.Color} Игрок ваш ход");

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.SetCursorPosition(60, 0);
                Console.WriteLine($"Ваше золото: {player.gold}");

                countStep++;

                SelectUnit(player, countStep);

            } while (player.Gold >= 20); 
        }

        private void SelectUnit(Player player, int countStep)
        {
            int typeUnit = 0;
            int inputX = 0, inputY = 0;

            if (player.Color == ConsoleColor.Red)
            {
                inputX = 2;
                inputY = countStep;
            }

            if (player.Color == ConsoleColor.Blue)
            {
                inputX = 18;
                inputY = countStep;
            }

            do
            {
                typeUnit = InputInt("Выберите героя:\n1.Воин(ХП=100ед. Урон 10ед. Цена 20)\n2.Маг(ХП=50ед. Урон 20ед. Цена 30)\n3.Лучник(ХП=60ед. Урон 15ед. Цена 25)\n");
            } while (typeUnit > 3 || typeUnit < 1);

            switch (typeUnit)
            {
                case 1:
                    player.BuyUnit(new Warrior(inputX, inputY));
                    break;
                case 2:
                    player.BuyUnit(new Wizard(inputX, inputY));
                    break;
                case 3:
                    player.BuyUnit(new Archer(inputX, inputY));
                    break;
            }
        }

        private int InputInt(string message)
        {
            bool inputResult;
            int numInt;
            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out numInt);
            } while (!inputResult);

            return numInt;
        }

        private void DrawBattlefield()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < xMax; i++)
            {
                Console.Write('*');
            }
            for (int i = 0; i < yMax; i++)
            {
                Console.WriteLine('*');
            }
            for (int i = 0; i < xMax; i++)
            {
                Console.Write('*');
            }

            for (int i = 0; i < yMax + 1; i++)
            {
                Console.SetCursorPosition(xMax, i);
                Console.WriteLine('*');
            }

        }

        private void DrawAllUnits(Player player) 
        {
            player.DrawUnits(player);
        }

        private void InfoAboutArmy(Player player) 
        {
            foreach (var unit in player.units)
            {
                Console.WriteLine($"у вас есть: " + unit);
            }
        }

        private void DrawAllElements(Player player, Player player2) 
        {
            Console.Clear();
            DrawAllUnits(player);
            DrawAllUnits(player2);
            DrawBattlefield();

            Console.SetCursorPosition(20, 17);
            Console.ForegroundColor = player.Color;
            Console.WriteLine($"{player.Color} Игрок ваш ход");
            InfoAboutArmy(player);
            Console.ReadKey();
        }

        private void Battle(Player player, Player player2)
        {
            do
            {
                DrawAllElements(player, player2);
                Player1Step(player, player2);
                DeleteDead(player2);
                Console.WriteLine("Ход окончен. Нажмите кнопку");
                Console.ReadKey();

                DrawAllElements(player2, player);
                Player2Step(player2, player);
                DeleteDead(player);
                Console.WriteLine("Ход окончен. Нажмите кнопку");
                Console.ReadKey();

            } while (player.units.Count != 0 && player2.units.Count != 0);

        }


        private void Player1Step(Player player, Player player2)
        {
            Console.ForegroundColor = player.Color;

            for (int i = 0; i < player.units.Count; i++)
            {
                for (int j = 0; j < player2.units.Count; j++)
                {
                    if ((player2.units[j].X - player.units[i].X) <= player.units[i].RangeAttack &&
                        (player2.units[j].Y - player.units[i].Y) <= player.units[i].RangeAttack)
                    {
                        player.units[i].Attack(player2.units[j]);
                        Console.WriteLine($"ваш {player.units[i].Name} атакует вражеского {player2.units[j].Name} и наносит {player.units[i].Damage} урона ");
                        break;
                    }

                    if ((player2.units[j].X - player.units[i].X) > player.units[i].RangeAttack)
                    {
                        player.units[i].MoveX(player.units[i]);
                        Console.WriteLine($"Враг далеко, ваш {player.units[i].Name} подходит ближе");
                        break;
                    }

                    if ((player2.units[j].Y - player.units[i].Y) > player.units[i].RangeAttack)
                    {
                        player.units[i].MoveY(player.units[i]);
                        Console.WriteLine($"Враг далеко, ваш {player.units[i].Name} подходит ближе");
                        break;
                    }
                }
            }
        }


        private void Player2Step(Player player2, Player player)
        {
            Console.ForegroundColor = player2.Color;

            for (int i = 0; i < player2.units.Count; i++)
            {
                for (int j = 0; j < player.units.Count; j++)
                {
                    if ((player2.units[i].X - player.units[j].X) <= player2.units[i].RangeAttack &&
                        (player2.units[i].Y - player.units[j].Y) <= player2.units[i].RangeAttack)
                    {
                        player2.units[i].Attack(player.units[j]);
                        Console.WriteLine($"ваш {player2.units[i].Name} атакует вражеского {player.units[j].Name} и наносит {player2.units[i].Damage} урона ");
                        break;
                    }

                    if ((player2.units[i].X - player.units[j].X) > player2.units[i].RangeAttack)
                    {
                        player2.units[i].MoveX2(player2.units[i]);
                        Console.WriteLine($"Враг далеко, ваш {player2.units[i].Name} подходит ближе");
                        break;
                    }

                    if ((player2.units[i].Y - player.units[j].Y) > player.units[i].RangeAttack)
                    {
                        player2.units[i].MoveY2(player2.units[i]);
                        Console.WriteLine($"Враг далеко,ваш {player2.units[i].Name} подходит ближе");
                        break;
                    }
                }
            }
        }


        private void DeleteDead(Player player)
        {
            for (int i = 0; i < player.units.Count; i++)
            {
                if (player.units[i].Hp <= 0)
                {
                    player.units.RemoveAt(i);
                }
            }

        }

        private void CheсkWinner(Player player, Player player2)
        {
            if (player.units.Count == 0)
            {
                Console.ForegroundColor = player2.Color;
                Console.WriteLine($"Победа {player2.Color} игрока");
            }
            if (player2.units.Count == 0)
            {
                Console.ForegroundColor = player.Color;
                Console.WriteLine($"Победа {player.Color} игрока");
            }
        }


        public void Play()
        {

            InterfaceBuyUnit(P1);

            InterfaceBuyUnit(P2);

            Battle(P1, P2);

            CheсkWinner(P1, P2);

        }

    }
}
